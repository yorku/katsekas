'use strict';

app.factory('SecurityResource', function ($resource) {
    return $resource('rest/security/:operation',
        {
            operation: '@operation'
        });
});