'use strict';

app.service('SecurityService', function ($log, SecurityResource) {
    this.check = function (callback) {
        return SecurityResource.get({operation: 'check'}, callback);
    };
});