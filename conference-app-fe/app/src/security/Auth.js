'use strict';

app.service('Auth', function ($rootScope) {
    $rootScope.authChecked = false;
    $rootScope.sessionId = null;

    this.isAuthenticated = function () {
        return $rootScope.sessionId != null;
    };

    this.isAuthChecked = function () {
        return $rootScope.authChecked;
    };

    this.markAuthChecked = function () {
        return $rootScope.authChecked = true;
    };

    this.setSessionId = function (sessionId) {
        return $rootScope.sessionId = sessionId;
    }
});