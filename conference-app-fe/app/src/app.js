'use strict';

var app = angular.module('conferenceApplication', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute',
  'ui.bootstrap',
  'ngFileUpload',
  'ngQuickDate'
]);

app.config(function ($routeProvider) {
  $routeProvider
      .when('/home', {
        templateUrl: '/src/unicorn/home/home.html',
        controller: 'HomeCtrl'
      })
      .when('/speakers', {
        templateUrl: '/src/unicorn/speaker/speaker-list.html',
        controller: 'SpeakerListCtrl'
      })
      .when('/sessions', {
        templateUrl: '/src/unicorn/session/session-list.html',
        controller: 'SessionListCtrl'
      })
      .when('/sessions/:sessionId', {
        templateUrl: '/src/unicorn/session/session-detail.html',
        controller: 'SessionDetailCtrl'
      })
      .when('/login', {
        templateUrl: '/src/unicorn/login/login.html',
        controller: 'LoginController'
      })
      .when('/not-authorized', {
        templateUrl: '/src/unicorn/home/not-authorized.html'
      })
      .otherwise({
        redirectTo: '/home'
      });
});

//Login validation.
//!!!!!! ATTENTION !!!!!!!
//If session id is expired, $rootScope.SessionID should be set to null or empty string!!!
app.run(function($rootScope, $location, SecurityService, Auth) {

  // register listener to watch route changes
  $rootScope.$on("$routeChangeStart", function(event, next, current) {
    if(!Auth.isAuthChecked() || (next.authenticated && !Auth.isAuthenticated())){
      SecurityService.check(function(login){
        Auth.markAuthChecked();
        if (login.sessionId ===  null || login.loggedIn === false) {
          Auth.setSessionId(null);
          if (next.authenticated){
            // no logged user, we should be going to #login
            if (next.templateUrl == "/src/unicorn/login/login.html") {
              // already going to #login, no redirect needed
            } else {
              // not going to #login, we should redirect now
              $location.path("/login");
            }
          }
        }else {
          Auth.setSessionId(login.sessionId);
        }
      })
    }
  });
});


