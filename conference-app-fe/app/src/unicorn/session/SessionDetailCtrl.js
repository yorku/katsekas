'use strict';

app.controller('SessionDetailCtrl', function ($scope, $routeParams, SessionService) {
    var sessionId = $routeParams.sessionId;
    $scope.session = SessionService.getById(sessionId);
});