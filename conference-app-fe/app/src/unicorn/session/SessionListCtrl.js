'use strict';

app.controller('SessionListCtrl', function ($scope, $log, $uibModal, $location, SessionService, SpeakerService) {
    $scope.sessions = SessionService.getAll(function(response){
        $scope.sessions = response;
    });

    var speakersForSelect = SpeakerService.getAll(function (response) {
        speakersForSelect.data = response;
        speakersForSelect.selected = function(list, selected){
            if(!list){
                return false;
            }
            for (var i = 0; i < list.length; i++) {
                if(list[i] == selected.id){
                    return true;
                }
            }
            return false;
        }
    });

    $scope.viewDetails = function (sessionId) {
        $location.path('/sessions/' + sessionId);
    };

    $scope.edit = function (session) {

        var sessionOrEmpty = session ? SessionService.getById(session.id, function(resp){
            sessionOrEmpty = resp;
            sessionOrEmpty.speakersForSelect = speakersForSelect;
        }) : {speakersForSelect: speakersForSelect};

        var modalInstance = $uibModal.open({
            templateUrl: '/src/unicorn/session/session-edit.html',
            authenticate: true,
            controller: 'ModalFormInstanceCtrl',
            windowClass: 'app-modal-window',
            resolve: {
                title: function () {
                    return session ? 'Change session' : 'Add session';
                },
                data: function () {
                    return sessionOrEmpty;
                }
            }
        });

        // peale modall-akna sulgemist teeme lehele täisuuenduse, vastasel juhul ei tööta enam kalendri lahendus
        modalInstance.result.then(function (data) {
            delete data.speakersForSelect;

            if (data.id) {
                SessionService.update(data, function (response) {
                    $log.info('Session updated at: ' + new Date());
                    var sessionIdx = $scope.sessions.indexOf(session);
                    $scope.sessions[sessionIdx] = response;
                });
            } else {
                SessionService.save(data, function (response) {
                    $log.info('Session saved at: ' + new Date());
                    $scope.sessions.push(response);
                });
            }
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.delete = function (session) {
        SessionService.delete(session.id, function () {
            $log.info('Session deleted at: ' + new Date());
            var sessionIdx = $scope.sessions.indexOf(session);
            $scope.sessions.splice(sessionIdx, 1);
        });
    };

    $scope.removeSpeaker = function (speakerId, session) {
        var index = session.speakers.indexOf(speakerId);
        session.speakersFull.splice(speakerId, 1);
        session.speakers.splice(speakerId, 1);
        SessionService.update(session, function (response) {
            $log.info('Session updated at: ' + new Date());
            var sessionIdx = $scope.sessions.indexOf(session);
            $scope.sessions[sessionIdx] = response;
        });
    };
});