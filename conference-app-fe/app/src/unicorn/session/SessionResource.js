'use strict';

app.factory('SessionResource', function ($resource) {
    return $resource('rest/session/:sessionId',
        {
            sessionId: '@sessionId'
        },
        {
            'update': {
                method: 'PUT'
            },
            'getBySpeakerId': {
                method: 'GET',
                url: 'rest/session/speaker/:speakerId',
                speakerId: '@speakerId',
                isArray: true
            }
        }
    )
});