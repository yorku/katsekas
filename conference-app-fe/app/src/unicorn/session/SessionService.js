'use strict';

app.service('SessionService', function ($log, SessionResource) {
    this.getById = function (id, callback) {
        return SessionResource.get({sessionId: id}, callback);
    };

    this.getBySpeakerId = function (speakerId, callback) {
        return SessionResource.getBySpeakerId({speakerId: speakerId}, callback);
    };

    this.getAll = function (callback) {
        return SessionResource.query(callback);
    };

    this.save = function (data, callback) {
        data = SessionResource.save(data, function () {
           callback(data);
        });
    }

    this.update = function (data, callback) {
        data = SessionResource.update(data, function () {
            callback(data);
        });
    }

    this.delete = function (id, callback) {
        return SessionResource.delete({sessionId: id}, callback);
    };
});