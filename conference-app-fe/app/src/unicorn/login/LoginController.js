'use strict';

app.controller('LoginController', function ($scope, $http, $location, $rootScope, Auth){

        $scope.user = {};
        $scope.user.username = '';
        $scope.user.password = '';

        $scope.loginUser = function(user) {
            $scope.resetError();
            $http.post('/j_spring_security_check', user).success(function(login) {
                if(login.sessionId ===  null || login.loggedIn === false) {
                    $scope.setError(login.status);
                    return;
                }

                $scope.user.username = '';
                $scope.user.password = '';

                Auth.setSessionId(login.sessionId);
                Auth.markAuthChecked();

                $location.path("home");

            }).error(function() {
                $scope.setError('Invalid user/password combination');
            });
        };

        $scope.resetError = function() {
            $scope.error = false;
            $scope.errorMessage = '';
        };

        $scope.setError = function(message) {
            $scope.error = true;
            $scope.errorMessage = message;
            $rootScope.SessionId = '';
        }
    }
);