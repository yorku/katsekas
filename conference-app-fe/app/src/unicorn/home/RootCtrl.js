'use strict';
var app = angular.module('conferenceApplication');

app.controller('RootCtrl', function ($scope, $rootScope, Auth, $location) {
    $scope.quickSearch = {};

    $scope.menuLinks = new Array(
        {"title": "Sessions", "url": "/#/sessions"},
        {"title": "Speakers", "url": "/#/speakers"}
    );

    $scope.isActive = function (view) {
        return view.substring(2) === $location.path();
    };

    $scope.isAuthenticated = Auth.isAuthenticated();

    $rootScope.$watch('sessionId', function() {
        $scope.isAuthenticated = Auth.isAuthenticated();
    });
});
