'use strict';

app.factory('SpeakerResource', function ($resource) {
    return $resource('rest/speaker/:speakerId',
        {
            speakerId: '@speakerId'
        },
        {
            'update': {
                method: 'PUT'
            }
        }
    )
});
