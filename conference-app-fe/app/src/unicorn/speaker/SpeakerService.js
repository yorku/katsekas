'use strict';

app.service('SpeakerService', function ($log, Upload, SpeakerResource, $http) {
    this.getById = function (id, callback) {
        return SpeakerResource.get({speakerId: id}, callback);
    };

    this.getAll = function (callback) {
        return SpeakerResource.query(callback);
    };

    this.save = function (data, callback) {
        var avatar = data.avatar;
        delete data.avatar;
        data = SpeakerResource.save(data, function () {
            saveAvatar(avatar, data, callback);
        });
    }

    this.update = function (data, callback) {
        var avatar = data.avatar;
        delete data.avatar;
        data = SpeakerResource.update(data, function () {
            saveAvatar(avatar, data, callback);
        });
    }

    function saveAvatar(avatar, data, callback) {
        if (avatar && data.id) {
            Upload.upload({
                url: 'rest/speaker/' + data.id + '/avatar',
                file: avatar,
                progress: function (e) {
                    $log.debug(e);
                }
            }).then(function (response, status, headers, config) {
                // file is uploaded successfully
                $log.info(response);
                callback(data);
            });
        } else {
            callback(data);
        }
    }

    this.delete = function (id, callback) {
        return SpeakerResource.delete({speakerId: id}, callback);
    };

    this.getPDF = function (callback) {
        $http.get(
            '/rest/speaker/pdf',
            {responseType: 'arraybuffer'}
        ).success(function (data) {
            callback(data);
        });
    };
});
