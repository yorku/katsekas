'use strict';

app.controller('SpeakerListCtrl', function ($scope, $uibModal, $log, $route, SpeakerService, SessionService) {
    $scope.speakers = SpeakerService.getAll(function (response) {
        $scope.speakers = response;
        for (var i = 0; i < $scope.speakers.length; i++) {
            $scope.speakers[i].avatarUri = getAvatarUri($scope.speakers[i].id);
        }
    });

    function getAvatarUri(speakerId) {
        return "rest/speaker/" + speakerId + "/avatar/small" + "?_ts=" + new Date().getTime();
    }

    function getAvatarMediumUri(speakerId) {
        return "rest/speaker/" + speakerId + "/avatar/medium";
    }

    $scope.edit = function (speaker) {
        var speakerOrEmpty = speaker ? SpeakerService.getById(speaker.id, function(resp){
            speakerOrEmpty = resp;
        }) : {};

        var modalInstance = $uibModal.open({
            templateUrl: '/src/unicorn/speaker/speaker-edit.html',
            authenticate: true,
            controller: 'ModalFormInstanceCtrl',
            resolve: {
                title: function () {
                    return speaker ? 'Change speaker' : 'Add speaker';
                },
                data: function () {
                    return speakerOrEmpty;
                }
            }
        });

        modalInstance.result.then(function (data) {
            if (data.id) {
                SpeakerService.update(data, function (response) {
                    $log.info('Speaker updated at: ' + new Date());
                    response.avatarUri = getAvatarUri(response.id);
                    var speakerIdx = $scope.speakers.indexOf(speaker);
                    $scope.speakers[speakerIdx] = response;
                });
            } else {
                SpeakerService.save(data, function (response) {
                    $log.info('Speaker saved at: ' + new Date());
                    response.avatarUri = getAvatarUri(response.id);
                    $scope.speakers.push(response);
                });
            }

        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.detailed = function (speaker) {
        speaker.avatarUri = getAvatarMediumUri(speaker.id);

        speaker.sessions = SessionService.getBySpeakerId(speaker.id, function (response) {
            speaker.sessions = response;
        });

        var modalInstance = $uibModal.open({
            templateUrl: '/src/unicorn/speaker/speaker-detail.html',
            controller: 'ModalFormInstanceCtrl',
            resolve: {
                title: function () {
                    return 'Speaker information';
                },
                data: function () {
                    return speaker;
                }
            }
        });
        $scope.modalInstance = modalInstance;

        modalInstance.result.then(function () {}, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };
    $scope.delete = function (speaker) {
        SpeakerService.delete(speaker.id, function () {
            $log.info('Speaker deleted at: ' + new Date());
            var speakerIdx = $scope.speakers.indexOf(speaker);
            $scope.speakers.splice(speakerIdx, 1);
        });
    };

    $scope.downloadPDF = function () {
        SpeakerService.getPDF(function (data) {
            var file = new Blob([data], {type: 'application/pdf'});
            var fileURL = URL.createObjectURL(file);
            var a = document.createElement('a');
            a.href = fileURL;
            a.download = "speakers.pdf";
            a.click();
            $log.info('Speaker list PDF recieved: ' + new Date());
        });
    };
});