'use strict';

app.controller('ModalFormInstanceCtrl', function ($scope, $uibModalInstance, $http, $log, title, data) {

    $scope.modalTitle = title;
    $scope.modalData = data;

    $scope.modalFormValid = false;

    $scope.setModalFormValidity = function(valid) {
        $scope.modalFormValid = valid;
    };

    $scope.ok = function () {
        $uibModalInstance.close($scope.modalData);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});