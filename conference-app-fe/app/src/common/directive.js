'use strict';

var app = angular.module('conferenceApplication');

/**
 * Attribute value can be used to determine if authentication is required or there cannot be authentication to see given element.
 * for-authenticated="true" means element is only visible when authenticated (default)
 * for-authenticated="false" means element is only visible without authentication
 */
app.directive('forAuthenticated', function (Auth) {
    function link($scope, element, attrs) {
        var requireAuthenticated = true; //Default is true
        if (attrs.hasOwnProperty('forAuthenticated') && attrs.forAuthenticated != "") {
            requireAuthenticated = attrs.forAuthenticated == "true";
        }
        var authenticated = Auth.isAuthenticated();
        var elementVisible = authenticated || !requireAuthenticated;

        if (!elementVisible) {
            element.hide();
        }
    }

    return {
        link: link,
        restrict: 'A'
    };
});

app.directive('activeLink', ['$location', function (location) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs, controller) {
            var clazz = attrs.activeLink;
            var path = attrs.href;
            scope.location = location;
            scope.$watch('location.path()', function (newPath) {
                if (path === newPath) {
                    element.parent().addClass(clazz);
                } else {
                    element.parent().removeClass(clazz);
                }
            });
        }
    };
}]);


function searchDirectiveController(scope, http, restUrl) {
    scope.searchResult = '';

    scope.search = function (val) {
        return http.get(restUrl, {
            params: {
                search: val
            }
        }).then(function (res) {
            var filter = [];
            angular.forEach(res.data, function (item) {
                filter.push(item);
            });
            return filter;
        });
    };

}

function searchDirectiveLink(scope, element, attrs, parse) {
    var input = element.find('input');
    var modelAccessor = parse(attrs.ngObjectModel);
    var modelSetter = modelAccessor.assign;

    if (attrs.required != undefined) {
        input.attr('required', '');
    }

    scope.$watch(modelAccessor, function (val) {
        scope.searchResult = val;

    });

    input.bind('blur', function () {
        scope.$apply(function () {
            modelSetter(scope, input.val());
        });
    });
}

app.directive('searchOrganization', function ($log, $parse) {
    return {
        restrict: 'E',
        templateUrl: 'src/common/modal/search-organization.html',
        controller: function ($scope, $http) {
            searchDirectiveController($scope, $http, 'rest/organization/');
        },
        link: function (scope, element, attrs) {
            searchDirectiveLink(scope, element, attrs, $parse);
        }
    };
});

app.directive('searchRoom', function ($log, $parse) {
    return {
        restrict: 'E',
        templateUrl: 'src/common/modal/search-room.html',
        controller: function ($scope, $http) {
            searchDirectiveController($scope, $http, 'rest/room/');
        },
        link: function (scope, element, attrs) {
            searchDirectiveLink(scope, element, attrs, $parse);
        }
    };
});

app.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var modelAccessor = $parse(attrs.fileModel);
            var modelSetter = modelAccessor.assign;
            element.bind('change', function () {
                scope.$apply(function () {
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);


app.directive('validFile',function(){
    return {
        require:'ngModel',
        link:function(scope,el,attrs,ngModel){
            el.bind('change',function(){
                scope.$apply(function(){
                    ngModel.$setViewValue(el.val());
                    ngModel.$render();
                });
            });
        }
    }
});
