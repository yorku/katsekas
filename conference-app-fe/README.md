# International Unicorn Research Agency - conference planning application 
### Story 
Here it is, the source code of our magical Unicorn seminar planning application filled with rainbows. Or actually it could use some more rainbows. Thing is that the contract between International Unicorn Research Agency and software developer, whose name we cannot disclose, was ended due to their constant jokes about Unicorns. We have a serious research agency and we just could not tolerate that. Now we are looking for volunteers who can help us fix this application so we could still use it to plan our yearly seminar in June. 
### Deployment guide 
The application is divided into two separate modules, one for the frontend(FE) and other for the backend(BE). To run the application you will need to install 
  - [Gradle] - for building the BE project
  - [NodeJS] (npm) - for FE project development dependency management
  - [Bower] - for FE project libraries management 
  - [Grunt] (with [command line tools]) - for running the FE project
### Backend
1.	Unpack conference-application.zip (recommended to import the project into Eclipse or other preferred IDE)
2.	Navigate to the conference-application project root directory 
3.	Run the MongoDB database server. If MongoDB is missing from path then downloads it - this may take a while.
```sh
	gradle startMongoDB
```
4. DB is running if you see something like: 
```sh
> Building 66% > :startMongoDB 
```
5.	This will run on port 27017. If you have issues with it then change the port in build.gradle and in rest-mongodb.xml 	This command will not complete - it will keep on running until stopped by user. 
6.	You could use some GUI like Robomongo to connect to that DB. 
    **PS! The database will be empty.**
7. Run tomcat:
```sh
gradle clean build tomcatRun
```
8.	This will run on port 8080 that can be changed in build.gradle as well. 
9.	After you see a message like "Started Tomcat Server" in console you can point your browser to http://localhost:8080/ to check if tomcat is running (should open apache tomcat 404 page)
### Frontend
1.	Unpack conference-app-fe.zip (recommended to import the project into WebStorm, SublimeText or other preferred IDE)
2.	Navigate to the conference-app-fe project root directory
3.	 Use npm (Node Package Manager) to install the first FE dependencies:
```sh
	$ npm install
```

4. Use bower to install the project libraries:
```sh
	$ bower install
```

5. If grunt-cli is installed correctly, you can now run the project:
```sh
	$ grunt serve
```

### Assignment 
Current application is functional but we need some improvements to be able to use it. These are the issues we would like you to solve: 
1.	There is some weird stuff happening when trying to save new speakers. The organization autocomplete does not work, the page needs to be reloaded manually after pressing OK in the modal and it is possible to add speakers without a picture. 
2.	Deleting sessions and speakers. We don't know why it was not implemented. We need to be able to delete sessions as well as specific speakers from our schedule. Logical delete is the preferred solution but removing them from db will also give near maximum score. 
3.	Speaker bio. Please add speaker biography to speaker edit form as well. 
4.	Currently there is no detailed view for speakers. We would like you to add it. 
a. On that detailed view we should be able to see all speaker fields (including bio). 
b. This view should be visible for everyone - not only for authenticated users (like edit). 
5.	On that detailed view show list of sessions that speaker is connected with - simple session summary will suffice but you can also add a link to session. 
6.	Avatar resize performance. It is not slow but we want it to be blazing fast, like Unicorns. The previous developer 
described us that currently 3 versions of images are saved to database. These 3 versions are created and saved sequentially - application creates small version and saves, then creates medium version and saves, etc. He also mumbled something about possibility to create these versions in parallel with ExecutorService. 
7.	Both Front-End and Back-End have issues with their libraries being too old. Update the back-end dependencies to use Java 1.8, latest Tomcat version, latest Spring version, etc. The front-end side has had some refreshments, but still the console complains about using deprecated api calls. The same might apply for the backend after updating libraries. Fix them.
8. The Gradle clean does not work as expected - everything generated should be deleted. Also find a way to clean the FE project via command line.

As previous developer ditched the code it may contain bugs. You may fix some of those as well but make sure you document in your e-mail what you fixed and where you made changes for those fixes. 

### Extra points
1. Being a (lazy) developer with an eye for optimization, you are welcome to try [Lombok]. If you choose to do so, please explain its benefits.
2. Write maintainable code!
3. For highlighting the active menu item
4. The name of the app/scripts directory does not reflect its contents very well. Points for renaming it to 'src' and still delivering a runnable application
5. Configure the BE project to log into a file
6. To show off your BE coding skills, implement a PDF generation of the list of speakers.


### Submitting your code 
1. Execute: gradle clean and clean the FE project
2. Zip your entire project dir and attach it to the email or upload it to dropbox, google drive (or some other decent file 
sharing service) and include the link in the email. 
3. E-mail this to the person who sent this assingment to you.
[//]: # 


   [Gradle]: <http://gradle.org/>
   [NodeJS]: <https://nodejs.org/en/>
   [Bower]: <http://bower.io/>
   [Grunt]: <http://gruntjs.com/>
   [command line tools]: <http://gruntjs.com/using-the-cli>
   [Lombok]: <https://projectlombok.org/>

