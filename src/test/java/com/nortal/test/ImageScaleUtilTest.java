package com.nortal.test;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;

import com.nortal.domain.SpeakerAvatarSize;
import com.nortal.util.ImageScaleUtil;

/**
 * @author Priit Liivak
 * 
 */
public class ImageScaleUtilTest {

  private static File outputFile = new File("src/test/resources/business-analyst-small.png");

  @Before
  public void setUp() {
    outputFile.delete();
  }

  @Test
  public void scaleTest() throws IOException {
    InputStream inputImageStream = new FileInputStream("src/test/resources/business-analyst.png");
    BufferedImage bufferedImage = ImageIO.read(inputImageStream);
    InputStream resultImageStream = ImageScaleUtil.scale(bufferedImage, SpeakerAvatarSize.small);
    IOUtils.copy(resultImageStream, new FileOutputStream(outputFile));
  }

}
