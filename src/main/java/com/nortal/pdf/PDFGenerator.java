package com.nortal.pdf;

import java.awt.Color;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDJpeg;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.nortal.domain.Speaker;

@Component
public class PDFGenerator {

	private static Logger LOG = LoggerFactory.getLogger(PDFGenerator.class);

	private PDDocument doc;
	private PDPageContentStream content;
	private PDPage page = new PDPage(PDPage.PAGE_SIZE_A4);

	private final String TITLE = "SPEAKERS";

	private final PDFont FONT = PDType1Font.HELVETICA;
	private final PDFont FONT_BOLD = PDType1Font.HELVETICA_BOLD;
	private final float TEXT_FONT_SIZE = 11;
	private final float TITLE_FONT_SIZE = 20;

	private final float LINE_HEIGHT = 12;
	private final float LINE_WIDTH = 170;

	private final float PAGE_HORIZONTAL_PADDING = 100;
	private final float PAGE_VERTICAL_PADDING = 70;

	private final float BOX_DISTANCE_FROM_TITLE = 80;
	private final float BOX_DISTANCE_FROM_OTHER_BOX = 15;
	private final float BOX_PADDING = 20;
	private final float BOX_WIDTH = page.getMediaBox().getWidth() - (PAGE_HORIZONTAL_PADDING * 2);

	private final float STARTING_Y_COORD = page.getMediaBox().getHeight() - PAGE_VERTICAL_PADDING
			- BOX_DISTANCE_FROM_TITLE - LINE_HEIGHT;
	private float currentSpeakerBoxHeight;

	private void calculateCurrentSpeakerBoxHeight(Speaker speaker) {
		currentSpeakerBoxHeight = (SpeakerBox.values().length + speaker.getBiography().getAchievements().size() - 1)
				* LINE_HEIGHT + (BOX_PADDING * 2);
	}

	public byte[] generatePDF(List<Speaker> speakers) {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		doc = new PDDocument();

		try {
			PDXObjectImage avatar = new PDJpeg(doc, new ByteArrayInputStream(speakers.get(0).getAvatar()));
			addPage();
			drawTitle();

			float speakerBoxY = STARTING_Y_COORD;
			for (Speaker speaker : speakers) {
				calculateCurrentSpeakerBoxHeight(speaker);
				if (speakerBoxY - currentSpeakerBoxHeight < PAGE_VERTICAL_PADDING) {
					addPage();
					speakerBoxY = STARTING_Y_COORD;
				}

				drawSpeakerBox(speaker, avatar, speakerBoxY);
				speakerBoxY -= currentSpeakerBoxHeight + BOX_DISTANCE_FROM_OTHER_BOX;
			}

			content.close();
			doc.save(output);
			doc.close();
		} catch (COSVisitorException | IOException e) {
			LOG.error("PDFGenerator#generatePDF", e);
		}
		return output.toByteArray();
	}

	private void addPage() throws IOException {
		if (content != null) {
			content.close();
		}
		page = new PDPage(PDPage.PAGE_SIZE_A4);
		doc.addPage(page);
		content = new PDPageContentStream(doc, page);
	}

	private void drawSpeakerBox(Speaker speaker, PDXObjectImage avatar, float speakerBoxY) throws IOException {
		drawSpeakerBoxContent(speaker, speakerBoxY);
		drawSpeakerBoxBorder(speakerBoxY);
	}

	private void drawSpeakerBoxBorder(float speakerBoxY) throws IOException {
		content.addRect(PAGE_HORIZONTAL_PADDING, speakerBoxY - currentSpeakerBoxHeight + BOX_PADDING + LINE_HEIGHT,
				BOX_WIDTH, currentSpeakerBoxHeight);
		content.setStrokingColor(Color.BLACK);
		content.setLineWidth(0.2f);
		content.stroke();
	}

	private enum SpeakerBox {
		NAME, ORGANIZATION, EDUCATION, ACHIEVEMENTS
	}

	private void drawSpeakerBoxContent(Speaker speaker, float speakerBoxY) throws IOException {
		content.beginText();
		content.moveTextPositionByAmount(PAGE_HORIZONTAL_PADDING + BOX_PADDING, speakerBoxY);
		drawLine(SpeakerBox.NAME.toString(), Collections.singletonList(speaker.getName()));
		drawLine(SpeakerBox.ORGANIZATION.toString(), Collections.singletonList(speaker.getOrganization().getName()));
		drawLine(SpeakerBox.EDUCATION.toString(), Collections.singletonList(speaker.getBiography().getEducation()));
		drawLine(SpeakerBox.ACHIEVEMENTS.toString(), speaker.getBiography().getAchievements());
		content.endText();
	}

	private void drawTitle() throws IOException {
		float titleWidth = FONT_BOLD.getStringWidth(TITLE) / 1000 * TITLE_FONT_SIZE;
		float titleHeight = FONT_BOLD.getFontDescriptor().getFontBoundingBox().getHeight() / 1000 * TITLE_FONT_SIZE;

		content.beginText();
		content.setFont(FONT_BOLD, TITLE_FONT_SIZE);
		content.moveTextPositionByAmount((page.getMediaBox().getWidth() - titleWidth) / 2,
				page.getMediaBox().getHeight() - titleHeight - PAGE_VERTICAL_PADDING);
		content.drawString(TITLE);
		content.endText();
	}

	private void drawLine(String title, final List<String> values) throws IOException {
		content.setFont(FONT_BOLD, TEXT_FONT_SIZE);
		content.drawString(title + ":");
		content.moveTextPositionByAmount(LINE_WIDTH, 0);

		content.setFont(FONT, TEXT_FONT_SIZE);
		for (String value : values) {
			content.drawString(value);
			content.moveTextPositionByAmount(0, -LINE_HEIGHT);
		}
		content.moveTextPositionByAmount(-LINE_WIDTH, 0);
	}
}