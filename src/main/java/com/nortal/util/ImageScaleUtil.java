package com.nortal.util;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import org.springframework.web.multipart.MultipartFile;

import com.nortal.domain.SpeakerAvatarSize;
import com.nortal.util.ImageScaler.ImageType;
import com.nortal.util.ImageScaler.ScalingDirection;

/**
 * 
 * @author Priit Liivak
 */
public class ImageScaleUtil {

  public static InputStream scale(MultipartFile multipartFile, SpeakerAvatarSize size) throws IOException {
    BufferedImage image = ImageIO.read(multipartFile.getInputStream());
    return scale(image, size);
  }

  public static InputStream scale(BufferedImage image, SpeakerAvatarSize size) throws IOException {
    int requiredWitdh = size.getWidth();
    int currentWidth = image.getWidth();
    if (SpeakerAvatarSize.original.equals(size) || currentWidth < requiredWitdh) {
      return getImageInputStream(image);
    }

    ImageScaler imageScaler = new ImageScaler(image);
    imageScaler.createScaledImage(requiredWitdh, ScalingDirection.HORIZONTAL);
    return getImageInputStream(imageScaler.getScaledImage(ImageType.IMAGE_PNG));
  }

  private static InputStream getImageInputStream(BufferedImage image) throws IOException {
    ByteArrayOutputStream os = new ByteArrayOutputStream();
    ImageIO.write(image, "png", os);
    return new ByteArrayInputStream(os.toByteArray());
  }

  /**
   * scale image. Source: http://stackoverflow.com/a/15558330/298824
   * 
   * @param sbi
   *          image to scale
   * @param imageType
   *          type of image
   * @param dWidth
   *          width of destination image
   * @param dHeight
   *          height of destination image
   * @param fWidth
   *          x-factor for transformation / scaling
   * @param fHeight
   *          y-factor for transformation / scaling
   * @return scaled image
   */
  private static BufferedImage scale(BufferedImage sbi, int imageType, int dWidth, int dHeight, double fWidth,
      double fHeight) {
    BufferedImage dbi = null;
    if (sbi != null) {
      dbi = new BufferedImage(dWidth, dHeight, imageType);
      Graphics2D g = dbi.createGraphics();
      AffineTransform at = AffineTransform.getScaleInstance(fWidth, fHeight);
      g.drawRenderedImage(sbi, at);
    }
    return dbi;
  }
}
