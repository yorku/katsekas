package com.nortal.util;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

/**
 * 
 * Simple bean to log out all runtime parameters. In development and test this
 * is helping to ensure correct parameter values have been set
 * 
 * @author Priit Liivak
 * 
 */
public class RuntimeParameterLogger implements InitializingBean {

	private static Logger LOG = LoggerFactory
			.getLogger(RuntimeParameterLogger.class);

	@Override
	public void afterPropertiesSet() throws Exception {
		if (!LOG.isInfoEnabled()) {
			return;
		}

		RuntimeMXBean bean = ManagementFactory.getRuntimeMXBean();
		List<String> aList = bean.getInputArguments();
		StringBuilder runtimeParams = new StringBuilder('\n');
		for (int i = 0; i < aList.size(); i++) {
			runtimeParams.append(aList.get(i));
			runtimeParams.append('\n');
		}
		LOG.info("Runtime parameres are as follows: " + runtimeParams);
	}

}
