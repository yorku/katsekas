package com.nortal.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.nortal.domain.security.Login;

public class CustomAuthenticationManager implements AuthenticationManager {

	private static Logger LOG = LoggerFactory
			.getLogger(CustomAuthenticationManager.class);

	private String username;
	private String password;

	public Authentication authenticate(Authentication auth)
			throws AuthenticationException {

		LOG.debug("Performing custom authentication");

		if (!username.equals(auth.getName())) {
			throw new BadCredentialsException("User does not exists!");
		}


		if (!password.equals(auth.getCredentials())) {
			throw new BadCredentialsException(
					"Entered username and password are the same!");
		} else {
			LOG.debug("User details are good and ready to go");
			UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
					auth.getName(),
					auth.getCredentials(), getAuthorities());

			Login login = new Login();
			login.setLoggedIn(true);
			token.setDetails(login);
			return token;
		}
	}

	public Collection<GrantedAuthority> getAuthorities() {
		// Create a list of grants for this user
		List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>(2);

		// All users are granted with ROLE_USER access
		// Therefore this user gets a ROLE_USER by default
		LOG.debug("Grant ROLE_USER to this user");
		authList.add(new SimpleGrantedAuthority("ROLE_USER"));

		// User has admin access
		LOG.debug("Grant ROLE_ADMIN to this user");
		authList.add(new SimpleGrantedAuthority("ROLE_ADMIN"));

		// Return list of granted authorities
		return authList;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
