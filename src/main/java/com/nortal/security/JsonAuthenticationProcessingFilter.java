package com.nortal.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nortal.domain.security.User;

public class JsonAuthenticationProcessingFilter extends
		AbstractAuthenticationProcessingFilter {

	private static Logger LOG = LoggerFactory
			.getLogger(JsonAuthenticationProcessingFilter.class);

	public JsonAuthenticationProcessingFilter() {
		super("/j_spring_security_check");
	}

	// ~ Methods
	// ========================================================================================================

	public Authentication attemptAuthentication(HttpServletRequest request,
			HttpServletResponse response) throws AuthenticationException {

		if (!request.getMethod().equals("POST")) {
			throw new AuthenticationServiceException(
					"Authentication method not supported: "
							+ request.getMethod());
		}

		ObjectMapper mapper = new ObjectMapper();

		User user = null;

		try {
			user = mapper.readValue(request.getInputStream(), User.class);
		} catch (Exception ex) {
			LOG.error(
					"JsonAuthenticationProcessingFilter#attemptAuthentication",
					ex);
			new InvalidJsonObjectException();
		}

		String username = user.getUsername();
		String password = user.getPassword();

		if (username == null) {
			username = "";
		}

		if (password == null) {
			password = "";
		}

		UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(
				username, password);

		// Allow subclasses to set the "details" property
		setDetails(request, authRequest);

		return this.getAuthenticationManager().authenticate(authRequest);
	}

	/**
	 * Provided so that subclasses may configure what is put into the
	 * authentication request's details property.
	 * 
	 * @param request
	 *            that an authentication request is being created for
	 * @param authRequest
	 *            the authentication request object that should have its details
	 *            set
	 */
	protected void setDetails(HttpServletRequest request,
			UsernamePasswordAuthenticationToken authRequest) {

		authRequest.setDetails(authenticationDetailsSource
				.buildDetails(request));
	}

}
