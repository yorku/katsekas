package com.nortal.repository;

import java.util.List;

import org.springframework.security.access.annotation.Secured;
import org.springframework.web.multipart.MultipartFile;

import com.nortal.domain.Speaker;
import com.nortal.domain.SpeakerAvatarSize;

public interface SpeakerService {

	List<Speaker> findAll();

	Speaker findById(String id);

	@Secured("ROLE_ADMIN")
	void update(Speaker speaker);

	@Secured("ROLE_ADMIN")
	void updateAvatar(MultipartFile multipartFile, String speakerId);

	byte[] getAvatar(String speakerId, SpeakerAvatarSize size);

	@Secured("ROLE_ADMIN")
	void insert(Speaker speaker);

	@Secured("ROLE_ADMIN")
	void delete(String id);

	byte[] getPDF();
}
