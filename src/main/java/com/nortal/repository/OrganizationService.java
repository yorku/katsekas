package com.nortal.repository;

import java.util.List;

import org.springframework.security.access.annotation.Secured;

import com.nortal.domain.Organization;

public interface OrganizationService {

  Organization findById(String id);

  List<Organization> findAll();

  List<Organization> findByName(String name);

  Organization getByName(String name);

  @Secured("ROLE_ADMIN")
  void update(Organization organization);

  @Secured("ROLE_ADMIN")
  void insert(Organization organization);
}
