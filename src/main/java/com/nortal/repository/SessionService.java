package com.nortal.repository;

import java.util.List;

import org.springframework.security.access.annotation.Secured;

import com.nortal.domain.Session;

public interface SessionService {

	List<Session> findAll();

	Session findById(String id);

	List<Session> findBySpeakerId(String speakerId);

	@Secured("ROLE_ADMIN")
	void update(Session session);

	@Secured("ROLE_ADMIN")
	void insert(Session session);

	@Secured("ROLE_ADMIN")
	void delete(String id);
}