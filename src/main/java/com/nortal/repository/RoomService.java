package com.nortal.repository;

import java.util.List;

import com.nortal.domain.Room;


public interface RoomService {

	Room findById(String id);

	List<Room> findAll();

	List<Room> findByName(String name);

	Room getByName(String name);

	void update(Room room);

	void insert(Room room);
}
