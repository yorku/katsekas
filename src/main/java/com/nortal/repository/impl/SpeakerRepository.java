package com.nortal.repository.impl;

import static org.springframework.data.mongodb.core.query.Criteria.where;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.mongodb.BasicDBObject;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSInputFile;
import com.nortal.domain.Organization;
import com.nortal.domain.Speaker;
import com.nortal.domain.SpeakerAvatarSize;
import com.nortal.pdf.PDFGenerator;
import com.nortal.repository.OrganizationService;
import com.nortal.repository.SpeakerService;
import com.nortal.util.ImageScaleUtil;

@Repository
public class SpeakerRepository extends CrudRepository implements SpeakerService {

	private static final String AVATAR_SIZE_FIELD = "size";
	private static final String AVATAR_SPEAKER_ID = "speakerId";
	private static final String SPEAKER_AVATAR_COLLECTION = "speakerAvatar";

	private static final String SPEAKER_ID = "id";
	private static final String SPEAKER_IS_DELETED = "isDeleted";

	private static Logger LOG = LoggerFactory.getLogger(SpeakerRepository.class);

	@Autowired
	private OrganizationService organizationService;

	@Autowired
	private PDFGenerator pdfGenerator;

	@Override
	// @Secured("ROLE_ADMIN")
	public List<Speaker> findAll() {
		List<Speaker> speakers = getTemplate().find(new Query(where(SPEAKER_IS_DELETED).is(false)), Speaker.class);
		LOG.debug("Loaded all speakers (" + speakers.size() + ")");

		for (Speaker speaker : speakers) {
			populate(speaker);
		}
		return speakers;
	}

	@Override
	public Speaker findById(String id) {
		Query query = new Query(where(SPEAKER_ID).is(id).and(SPEAKER_IS_DELETED).is(false));
		Speaker speaker = getTemplate().findOne(query, Speaker.class);
		if (speaker != null) {
			populate(speaker);
		}
		return speaker;
	}

	@Override
	public void update(Speaker speaker) {
		saveOrganization(speaker);
		getTemplate().save(speaker);
	}

	@Override
	public void updateAvatar(MultipartFile multipartFile, String speakerId) {
		GridFS avatarGfs = new GridFS(getTemplate().getDb(), SPEAKER_AVATAR_COLLECTION);
		avatarGfs.remove(new BasicDBObject(AVATAR_SPEAKER_ID, speakerId));

		ExecutorService executor = Executors.newFixedThreadPool(SpeakerAvatarSize.values().length);
		CompletionService<String> compService = new ExecutorCompletionService<>(executor);

		for (final SpeakerAvatarSize size : SpeakerAvatarSize.values()) {
			Runnable task = () -> {
				try {
					saveAvatar(multipartFile, speakerId, size, avatarGfs);
					if (size.equals(SpeakerAvatarSize.small) && executor.isShutdown()) {
						executor.awaitTermination(1, TimeUnit.MILLISECONDS);
						return;
					}
				} catch (IOException | InterruptedException e) {
					LOG.error("SpeakerRepository#updateAvatar", e);
				}
			};
			compService.submit(task, "");
		}

		executor.shutdown();
		try {
			executor.awaitTermination(1000, TimeUnit.MILLISECONDS);
		} catch (InterruptedException e) {
			LOG.error("SpeakerRepository#updateAvatar", e);
		}
	}

	private void saveAvatar(MultipartFile multipartFile, String speakerId, SpeakerAvatarSize size, GridFS avatarGfs)
			throws IOException {
		InputStream originalInputStream = ImageScaleUtil.scale(multipartFile, size);

		GridFSInputFile gfsFile = avatarGfs.createFile(originalInputStream, true);
		gfsFile.setFilename(multipartFile.getOriginalFilename());
		gfsFile.put(AVATAR_SPEAKER_ID, speakerId);
		gfsFile.put(AVATAR_SIZE_FIELD, size.getValue());
		gfsFile.save();
	}

	@Override
	public byte[] getAvatar(String speakerId, SpeakerAvatarSize size) {
		GridFS gfsPhoto = new GridFS(getTemplate().getDb(), SPEAKER_AVATAR_COLLECTION);
		BasicDBObject query = new BasicDBObject(AVATAR_SPEAKER_ID, speakerId);
		query.put(AVATAR_SIZE_FIELD, size.getValue());

		GridFSDBFile file = gfsPhoto.findOne(query);
		if (file == null) {
			return null;
		}
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int read = 0;
		InputStream inputStream = file.getInputStream();
		try {
			while ((read = inputStream.read(buffer, 0, buffer.length)) != -1) {
				baos.write(buffer, 0, read);
			}
			inputStream.close();
			baos.flush();
		} catch (IOException e) {
			LOG.error("SpeakerRepository#getAvatar", e);
		}
		return baos.toByteArray();
	}

	@Override
	public void insert(Speaker speaker) {
		saveOrganization(speaker);
		getTemplate().insert(speaker);
	}

	private void saveOrganization(Speaker speaker) {
		if (StringUtils.isEmpty(speaker.getOrganization().getName())) {
			speaker.setOrganizationId(null);
			return;
		}

		Organization find = organizationService.getByName(speaker.getOrganization().getName());
		if (find == null) {
			speaker.getOrganization().setId(null);
			organizationService.insert(speaker.getOrganization());
		} else {
			speaker.setOrganization(find);
		}

		speaker.setOrganizationId(speaker.getOrganization().getId());
	}

	private void populate(Speaker speaker) {
		if (!StringUtils.isEmpty(speaker.getOrganizationId())) {
			speaker.setOrganization(organizationService.findById(speaker.getOrganizationId()));
		}
	}

	private void populateWithAvatar(Speaker speaker) {
		speaker.setAvatar(getAvatar(speaker.getId(), SpeakerAvatarSize.small));
	}

	@Override
	public void delete(String id) {
		getTemplate().updateFirst( //
				new Query(where(SPEAKER_ID).is(id).and(SPEAKER_IS_DELETED).is(false)), //
				Update.update(SPEAKER_IS_DELETED, true), //
				Speaker.class);
	}

	@Override
	public byte[] getPDF() {
		List<Speaker> speakers = findAll();
		speakers.stream().forEach((speaker) -> {
			populateWithAvatar(speaker);
		});
		return pdfGenerator.generatePDF(speakers);
	}
}