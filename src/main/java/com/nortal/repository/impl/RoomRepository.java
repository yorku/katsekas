package com.nortal.repository.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.nortal.domain.Room;
import com.nortal.repository.RoomService;

@Repository
public class RoomRepository extends CrudRepository implements RoomService {

	private static Logger LOG = LoggerFactory.getLogger(RoomRepository.class);

	@Override
	public Room findById(String id) {
		return getTemplate().findById(id, Room.class);
	}

	@Override
	public List<Room> findAll() {
		return getTemplate().findAll(Room.class);
	}

	@Override
	public List<Room> findByName(String name) {
		Query query = new Query();
		query.addCriteria(Criteria.where("name").regex(name));
		return getTemplate().find(query, Room.class);
	}

	@Override
	public Room getByName(String name) {
		Query query = new Query();
		query.addCriteria(Criteria.where("name").is(name));
		return getTemplate().findOne(query, Room.class);
	}

	@Override
	public void update(Room room) {
		getTemplate().save(room);
	}

	@Override
	public void insert(Room room) {
		getTemplate().insert(room);
	}
}
