package com.nortal.repository.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;

import com.nortal.domain.Organization;
import com.nortal.repository.OrganizationService;

@Repository
public class OrganizationRepository extends CrudRepository implements
		OrganizationService {
	
	private static Logger LOG = LoggerFactory
			.getLogger(OrganizationRepository.class);

	@PreAuthorize("hasRole('ROLE_AaaDMIN')")
	@Override
	public Organization findById(String id) {
		return getTemplate().findById(id, Organization.class);
	}

	@Override
	public List<Organization> findAll() {
        return getTemplate().findAll(Organization.class);
    }

	@Override
	public List<Organization> findByName(String name) {
		Query query = new Query();
		query.addCriteria(Criteria.where("name").regex(name));
		return getTemplate().find(query, Organization.class);
	}
    
	@Override
	public Organization getByName(String name) {
		Query query = new Query();
		query.addCriteria(Criteria.where("name").is(name));
		return getTemplate().findOne(query, Organization.class);
	}

	@Override
    public void update(Organization organization) {
        getTemplate().save(organization);
    }
    
	@Override
    public void insert(Organization organization) {
        getTemplate().insert(organization);
    }
}
