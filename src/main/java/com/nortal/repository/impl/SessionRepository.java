package com.nortal.repository.impl;

import static org.springframework.data.mongodb.core.query.Criteria.where;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.nortal.domain.Room;
import com.nortal.domain.Session;
import com.nortal.domain.Speaker;
import com.nortal.repository.RoomService;
import com.nortal.repository.SessionService;
import com.nortal.repository.SpeakerService;

@Repository
public class SessionRepository extends CrudRepository implements SessionService {

	private static Logger LOG = LoggerFactory.getLogger(SessionRepository.class);

	private static final String SESSION_ID = "id";
	private static final String SESSIONIS_IS_DELETED = "isDeleted";
	private static final String SESSION_SPEAKERS = "speakers";

	@Autowired
	private RoomService roomService;
	@Autowired
	private SpeakerService speakerService;

	@Override
	public List<Session> findAll() {
		List<Session> sessions = getTemplate().find(new Query(where(SESSIONIS_IS_DELETED).is(false)), Session.class);

		for (Session session : sessions) {
			populate(session);
		}

		return sessions;
	}

	@Override
	public Session findById(String id) {
		Query query = new Query(where(SESSION_ID).is(id).and(SESSIONIS_IS_DELETED).is(false));
		Session session = getTemplate().findOne(query, Session.class);
		if (session != null) {
			populate(session);
		}

		return session;
	}

	@Override
	public List<Session> findBySpeakerId(String speakerId) {
		Query query = new Query(Criteria.where(SESSION_SPEAKERS).is(speakerId).and(SESSIONIS_IS_DELETED).is(false));
		List<Session> sessions = getTemplate().find(query, Session.class);
		for (Session session : sessions) {
			populate(session);
		}
		return sessions;
	}

	@Override
	public void update(Session session) {
		saveRoom(session);
		getTemplate().save(session);
		populate(session);
	}

	@Override
	public void insert(Session session) {
		saveRoom(session);
		getTemplate().insert(session);
		populate(session);
	}

	private void saveRoom(Session session) {
		if (StringUtils.isEmpty(session.getRoom().getName())) {
			session.setRoomId(null);
			return;
		}

		Room find = roomService.getByName(session.getRoom().getName());
		if (find == null) {
			session.getRoom().setId(null);
			roomService.insert(session.getRoom());
		} else {
			session.setRoom(find);
		}

		session.setRoomId(session.getRoom().getId());
	}

	private void populate(Session session) {
		if (!StringUtils.isEmpty(session.getRoomId())) {
			session.setRoom(roomService.findById(session.getRoomId()));
		}

		List<Speaker> speakers = new ArrayList<Speaker>();

		for (String speakerId : session.getSpeakers()) {
			Speaker speaker = speakerService.findById(speakerId);
			if (speaker != null) {
				speakers.add(speaker);
			}
		}
		session.setSpeakersFull(speakers);
	}

	@Override
	public void delete(String id) {
		getTemplate().updateFirst( //
				new Query(where(SESSION_ID).is(id).and(SESSIONIS_IS_DELETED).is(false)), //
				Update.update(SESSIONIS_IS_DELETED, true), //
				Session.class);
	}
}
