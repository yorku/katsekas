package com.nortal.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.nortal.domain.Room;
import com.nortal.repository.RoomService;

@Controller
@RequestMapping("/room")
public class RoomResource {


	@Autowired
	private RoomService roomService;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Room>> search(
			@RequestParam(value = "search", required = true) String search) {
		List<Room> find = roomService.findByName(search);

		return new ResponseEntity<List<Room>>(find, HttpStatus.ACCEPTED);
	}

}
