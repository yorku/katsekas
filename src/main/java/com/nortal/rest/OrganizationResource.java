package com.nortal.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.nortal.domain.Organization;
import com.nortal.repository.OrganizationService;

@Controller
@RequestMapping("/organization")
public class OrganizationResource {

	private static Logger LOG = LoggerFactory
			.getLogger(OrganizationResource.class);

	@Autowired
	private OrganizationService organizationService;


	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Organization>> search(
			@RequestParam(value = "search", required = true) String search) {
		List<Organization> find = organizationService.findByName(search);
		
		return new ResponseEntity<List<Organization>>(find, HttpStatus.ACCEPTED);
	}

}
