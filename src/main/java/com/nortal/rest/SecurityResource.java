package com.nortal.rest;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.nortal.domain.security.Login;

@Controller
@RequestMapping("/security")
public class SecurityResource {

	@RequestMapping(value = "/check", method = RequestMethod.GET)
	public ResponseEntity<Login> check(HttpServletRequest request) {

		Login login;
		if (SecurityContextHolder.getContext().getAuthentication().getDetails() instanceof Login) {
			login = (Login) SecurityContextHolder.getContext()
					.getAuthentication().getDetails();
			login.setSessionId(request.getSession().getId());
		} else {
			login = new Login();
		}

		return new ResponseEntity<Login>(login, HttpStatus.ACCEPTED);

	}

	@RequestMapping(value = "/login-failed", method = RequestMethod.GET)
	public ResponseEntity<Login> loginFailed(HttpServletRequest request) {

		Login login = new Login();
		login.setStatus("Login failed");

		return new ResponseEntity<Login>(login, HttpStatus.ACCEPTED);

	}

}
