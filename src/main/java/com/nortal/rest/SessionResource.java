package com.nortal.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.nortal.domain.Session;
import com.nortal.repository.SessionService;

@Controller
@RequestMapping("/session")
public class SessionResource {

    private static Logger LOG = LoggerFactory.getLogger(SessionResource.class);

    @Autowired
	private SessionService sessionService;


	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Session>> getAll() {
		List<Session> sessions = sessionService.findAll();
		return new ResponseEntity<List<Session>>(sessions, HttpStatus.ACCEPTED);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public ResponseEntity<Session> get(@PathVariable String id) {
		Session session = sessionService.findById(id);
		return new ResponseEntity<Session>(session, HttpStatus.ACCEPTED);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/speaker/{speakerId}")
	public ResponseEntity<List<Session>> getBySpeakerId(@PathVariable String speakerId) {
		List<Session> sessions = sessionService.findBySpeakerId(speakerId);
		return new ResponseEntity<List<Session>>(sessions, HttpStatus.ACCEPTED);
	}

	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<Session> update(@RequestBody Session session) {
		sessionService.update(session);
		return new ResponseEntity<Session>(session, HttpStatus.ACCEPTED);
    }


	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Session> insert(@RequestBody Session session) {
		sessionService.insert(session);
		return new ResponseEntity<Session>(session, HttpStatus.ACCEPTED);
    }

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public ResponseEntity<String> delete(@PathVariable String id) {
		sessionService.delete(id);
		return new ResponseEntity<String>(HttpStatus.ACCEPTED);
	}
}
