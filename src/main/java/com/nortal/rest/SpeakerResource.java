package com.nortal.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.nortal.domain.Speaker;
import com.nortal.domain.SpeakerAvatarSize;
import com.nortal.repository.SpeakerService;

@Controller
@RequestMapping("/speaker")
public class SpeakerResource {

  @Autowired
  private SpeakerService speakerService;
	private static Logger LOG = LoggerFactory.getLogger(SpeakerResource.class);

  @RequestMapping(method = RequestMethod.GET)
  public ResponseEntity<List<Speaker>> getAll() {
    List<Speaker> speakers = speakerService.findAll();
    return new ResponseEntity<List<Speaker>>(speakers, HttpStatus.ACCEPTED);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{id}")
  public ResponseEntity<Speaker> get(@PathVariable String id) {
    Speaker speaker = speakerService.findById(id);
    return new ResponseEntity<Speaker>(speaker, HttpStatus.ACCEPTED);
  }

  @RequestMapping(method = RequestMethod.PUT)
  public ResponseEntity<Speaker> update(@RequestBody Speaker speaker) {
    speakerService.update(speaker);
    return new ResponseEntity<Speaker>(speaker, HttpStatus.ACCEPTED);
  }


  @RequestMapping(method = RequestMethod.POST)
  public ResponseEntity<Speaker> insert(@RequestBody Speaker speaker) {
    speakerService.insert(speaker);
    return new ResponseEntity<Speaker>(speaker, HttpStatus.ACCEPTED);
  }
	@RequestMapping(method = RequestMethod.POST, value = "/{speakerId}/avatar", produces = "application/json")
	public ResponseEntity<String> updateAvatar(@RequestPart("file") MultipartFile multipartFile,
			@PathVariable("speakerId") String speakerId) {
		speakerService.updateAvatar(multipartFile, speakerId);
		LOG.info("speakerId");
		String json = "{\"speakerId\":\"" + speakerId + "\"}";
		return new ResponseEntity<String>(json, HttpStatus.ACCEPTED);
	}


	@ResponseBody
	@RequestMapping(method = RequestMethod.GET, value = "/{speakerId}/avatar/{size}", produces = MediaType.IMAGE_JPEG_VALUE)
	public byte[] getAvatar(@PathVariable("speakerId") String speakerId, @PathVariable("size") SpeakerAvatarSize size) {
		return speakerService.getAvatar(speakerId, size);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public ResponseEntity<String> delete(@PathVariable String id) {
		speakerService.delete(id);
		return new ResponseEntity<String>(HttpStatus.ACCEPTED);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/pdf")
	public ResponseEntity<byte[]> getPDF() {
		byte[] pdf = speakerService.getPDF();

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.parseMediaType("application/pdf"));
		headers.set("Content-disposition", "attachment; filename==\"speakers.pdf\"");
		return new ResponseEntity<byte[]>(pdf, headers, HttpStatus.OK);
	}
}