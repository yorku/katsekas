package com.nortal.domain;

import java.io.Serializable;

public enum SpeakerAvatarSize implements Serializable {

  small(100), medium(300), original(-1);

  /**
   * Width in pixels. 
   */
  private int width;

  private SpeakerAvatarSize(int width) {
    this.width = width;
  }

  public int getWidth() {
    return width;
  }

  public String getValue() {
    return name().toLowerCase();
  }
}
