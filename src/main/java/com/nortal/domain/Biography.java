package com.nortal.domain;

import java.util.List;

import lombok.Data;

@Data
public class Biography {

	private String education;
	private List<String> achievements;
}
