package com.nortal.domain;

import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Document(collection = "session")
public class Session {

	@Id
	private String id;
	private Date startDate;
	private Date endDate;
	private String subject;
	private String description;

	@Transient
	private List<Speaker> speakersFull;
	private List<String> speakers;

	@Transient
	private Room room;
	private String roomId;

	@JsonIgnore
	private boolean isDeleted;
}