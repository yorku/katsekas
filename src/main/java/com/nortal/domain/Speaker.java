package com.nortal.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Document(collection = "speaker")
public class Speaker {

	@Id
	private String id;
	private String name;
	private String organizationId;
	private Biography biography;

	@Transient
	private Organization organization;

	@JsonIgnore
	private boolean isDeleted;

	@Transient
	@JsonIgnore
	private byte[] avatar;
}